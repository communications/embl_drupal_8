# Get started

To use this theme, you need to install the 8.x-6.0 branch of the [ZURB Foundation Drupal theme](https://www.drupal.org/project/zurb_foundation).


## Manual sub-theme setup

 3. Edit your sub-theme to use the proper function names.


Optional steps:

 7. Modify the markup in Foundation core theme's template files.

    If you decide you want to modify any of the .html.twig template files in the
    zurb_foundation folder, copy them to your sub-theme's folder before
    making any changes.And then rebuild the theme registry.

    For example, copy zurb_foundation/templates/page.html.twig to
    THEMENAME/templates/page.html.twig.

 8. Optionally override existing Drupal core *.html.twig templates in your sub-theme.

 9. Add custom css and js files to your sub-theme


    You'll also need to edit your info.yml file to include your new library.
    There are instructions in the info.yml file to help you do this.

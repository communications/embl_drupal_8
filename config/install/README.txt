OPTIONAL DEFAULT SETTINGS
----------------------------------

If you need to implement custom settings in your theme, you can place a file
named embl_drupal_8.settings.yml here, where "embl_drupal_8" is the name of your sub-theme.

See zurb-foundation/config/install/zurb_foundation.settings.yml for example.